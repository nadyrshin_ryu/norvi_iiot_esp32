#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ModbusMaster.h>

#define MAX485_SerialNum  0
#define MAX485_DE         4
#define MAX485_TX         25
#define MAX485_RX         3//5

#define MODBUS_BAUD       2400
#define MODBUS_SLAVE_ID   1

int OkCount = 0;
int ErrCount = 0;

Adafruit_SSD1306 display(128, 64, &Wire, -1);
HardwareSerial RsSerial(MAX485_SerialNum);

ModbusMaster node;

void preTransmission()
{
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_DE, 0);
}

void setup()
{
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();
  display.setTextColor(SSD1306_WHITE);

  pinMode(MAX485_DE, OUTPUT);
  digitalWrite(MAX485_DE, 0);
  RsSerial.begin(MODBUS_BAUD, SERIAL_8N1, MAX485_RX, MAX485_TX);

  node.begin(MODBUS_SLAVE_ID, RsSerial);
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);
}

float Convert_2Regs_ToFloat(uint16_t reg1, uint16_t reg2)
{
  uint16_t buff[2];
  buff[0] = reg2;
  buff[1] = reg1;
  return *((float *)buff);
}

void loop()
{
  float voltage, current, power, freq, pf;
  uint8_t result;

  result = node.readInputRegisters(0, 14);
  if (result == node.ku8MBSuccess)
  {
    voltage = Convert_2Regs_ToFloat(node.getResponseBuffer(0), node.getResponseBuffer(1));
    current = Convert_2Regs_ToFloat(node.getResponseBuffer(6), node.getResponseBuffer(7));
    power = Convert_2Regs_ToFloat(node.getResponseBuffer(6), node.getResponseBuffer(7));
    OkCount++;
  }
  else
    ErrCount++;

  result = node.readInputRegisters(30, 2);
  if (result == node.ku8MBSuccess)
  {
    pf = Convert_2Regs_ToFloat(node.getResponseBuffer(0), node.getResponseBuffer(1));
    OkCount++;
  }
  else
    ErrCount++;

  result = node.readInputRegisters(70, 2);
  if (result == node.ku8MBSuccess)
  {
    freq = Convert_2Regs_ToFloat(node.getResponseBuffer(0), node.getResponseBuffer(1));
    OkCount++;
  }
  else
    ErrCount++;

  display.clearDisplay();
  display.setTextSize(1);

  display.setCursor(0, 0);
  display.print("OK: "); 
  display.print(OkCount);

  display.setCursor(64, 0);
  display.print("ERR: "); 
  display.print(ErrCount);

  display.setCursor(0, 16);
  display.print("Voltage: "); 
  display.print(voltage);

  display.setCursor(0, 26);
  display.print("Current: "); 
  display.print(current);

  display.setCursor(0, 36);
  display.print("Power: "); 
  display.print(power);

  display.setCursor(0, 46);
  display.print("PF: "); 
  display.print(pf);

  display.setCursor(0, 56);
  display.print("Freq: "); 
  display.print(freq);

  display.display();
}
